/*
  ColourCam
  This sketch reads Colour data from the on-board APDS9960 sensor of the
  Nano 33 BLE Sense and stores the 'image' in EEPROM (not implemted yet).

  This code is part of the OpenDoTT project, subject to the exploration of Wearables and the Self.
*/

#include <Arduino_APDS9960.h>

const int SIGNAL_LED = 13;

const unsigned int FULL_BRIGHTNESS = 65535;
const unsigned int HALF_BRIGHTNESS = 65535 >> 1;

// for the RGB SIGNAL_LED
const int R_LED_PIN = LEDR;
const int G_LED_PIN = LEDG;
const int B_LED_PIN = LEDB;

void setRGB(int r, int g, int b) {
    analogWrite(R_LED_PIN, 255-r);
    analogWrite(G_LED_PIN, 255-g);
    analogWrite(B_LED_PIN, 255-b);
}

void RGBoff() {
    setRGB(0,0,0);
}

void setup()
{
    pinMode(SIGNAL_LED, OUTPUT);
    digitalWrite(SIGNAL_LED, LOW);

    pinMode(R_LED_PIN, OUTPUT);
    pinMode(G_LED_PIN, OUTPUT);
    pinMode(B_LED_PIN, OUTPUT);
    RGBoff();

    Serial.begin(9600);
    while (!Serial)
        ;

    if (!APDS.begin())
    {
        Serial.println("Error initializing APDS9960 sensor.");
    }
}

void loop()
{
    if (!APDS.proximityAvailable())
    {
        Serial.println("No proximity available.");
        delay(3000);
        return; // start over
    }

    // From the documentation
    // read the proximity
    // - 0   => close
    // - 255 => far
    // - -1  => error
    int proximity = APDS.readProximity();

    // print value to the Serial Monitor
    Serial.println(proximity);
    if (proximity != 0) {
        Serial.print("No object detected.");
        Serial.println(proximity);
        delay(3000);
        return; // start over
    }

    // check if a color reading is available
    while (!APDS.colorAvailable())
    {
        delay(5);
    }
    int r, g, b;

    // read the color
    APDS.readColor(r, g, b);

    // print the values
    Serial.print("r = ");
    Serial.println(r);
    Serial.print("g = ");
    Serial.println(g);
    Serial.print("b = ");
    Serial.println(b);
    Serial.println();

    Serial.println("Scan successfull. Press reset to take another one.");
    digitalWrite(SIGNAL_LED, HIGH);
    setRGB(
        map(r, 0, HALF_BRIGHTNESS, 0, 255),
        map(g, 0, HALF_BRIGHTNESS, 0, 255),
        map(b, 0, HALF_BRIGHTNESS, 0, 255)
    );

    // hold in an infinite loop
    while(1) {
        delay(5000);
    };
}