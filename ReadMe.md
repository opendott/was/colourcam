# ColourCam

A simple camera like object using the colour and prximity sensor of the [Arduino Nano BLE Sense][].

The aim os to build a simple camera to take single colour images of any object. The object has to be close enough to make a _colour scan_. The _images_ are stored in EPROM and (at later stage) can be received on a smartphone with an app.

## Sensor Readings

### Colour

Colour values come in as 16bit values 0 - 65535.

### Proximity

0 means object close; -1 nothing detected; 1-255 distance of object detected.

## Board Info

The BLE sense board features a range of onboard sensor and a Nordic nRF52 chip with BLE.

The [Arduino Nano BLE Sense Guide][] has more information how to work with the board.

[Arduino Nano BLE Sense]: https://store.arduino.cc/arduino-nano-33-ble-sense
[Arduino Nano BLE Sense guide]: https://www.arduino.cc/en/Guide/NANO33BLESense